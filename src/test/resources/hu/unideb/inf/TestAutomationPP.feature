Feature: YourLogo Sing UP Page

  Background:
    Given The home page is opened
    And The Sign In is clicked


  Scenario Outline:
    Given The '<field>' is filled in with '<value>'
    And The Sign In Button is clicked
    Then The '<msg>' is shown
    Examples:
      | field | value             | msg                        |
      | email | invalid.email.com | Invalid email address.     |
      | email | valid@email.com   | Password is required.      |
      | email |                   | An email address required. |